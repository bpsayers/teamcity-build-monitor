require 'selenium-webdriver'

the_world = nil

World do
  the_world = TeamCityWorld.new
end

at_exit do
  the_world.close
end