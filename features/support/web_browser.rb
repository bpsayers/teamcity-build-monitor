class WebBrowser
  def initialize
    profile = Selenium::WebDriver::Firefox::Profile.new
    profile.add_extension 'artifacts/firefox-plugin/teamcity-build-monitor.xpi'
    @browser = Selenium::WebDriver.for :firefox, :profile => profile
  end

  def web_driver
    @browser
  end

  def navigate_to(url)
    @browser.navigate.to url
  end

  def close
    @browser.quit
  end
end