var pageMod = require("page-mod");
pageMod.PageMod({
    include: "http://192.168.1.85:8111/build-monitor",
    contentScriptFile: require('self').data.url('test.js')
});

require("widget").Widget({
    id: "team-city-build-monitor",
    label: "TeamCity Build Monitor",
    content: "TC-BM",
    width: 50,
    onClick: function () {
        var tabs = require('tabs');
        tabs.activeTab.url = 'http://192.168.1.85:8111/build-monitor';
    }
});