require 'rake'
require 'cucumber'
require 'cucumber/rake/task'
require 'fileutils'

root_path = File.expand_path '.'
firefox_plugin_output_path = "#{root_path}/artifacts/firefox-plugin"
package_json_template_path = "#{root_path}/tasks/package.json.template"
lib_path = "#{root_path}/lib"
data_path = "#{root_path}/data"
package_json_output_path = "#{firefox_plugin_output_path}/package.json"
cfx = "#{root_path}/tasks/addon-sdk-1.9/bin/cfx"

def replace_in_file(original_string, replacement_string, input_file, output_file=input_file)
  template = File.read input_file
  content = template.gsub(original_string, replacement_string)
  File.open(output_file, 'w') { |file| file.puts content }
end

task :default => [:features]

desc 'Removes the artifacts directory'
task :clean do
  FileUtils.remove_dir 'artifacts', true
end

desc 'Compiles the code'
task :compile, [:version] => [:clean] do |t, args|
  args.with_defaults(version: '0.0.0.0')

  FileUtils.makedirs firefox_plugin_output_path
  FileUtils.cp_r [data_path, lib_path], firefox_plugin_output_path

  replace_in_file('@@version@@', args.version, package_json_template_path, package_json_output_path)
end

desc 'Generates the firefox xpi package'
task :generate_xpi, [:version] => [:compile] do
  Dir.chdir firefox_plugin_output_path
  %x[#{cfx} xpi]
  Dir.chdir root_path
end

desc 'Runs firefox with the plugin installed'
task :manual_test, [:version] => [:compile] do
  Dir.chdir firefox_plugin_output_path
  %x[#{cfx} run]
  Dir.chdir root_path
end

desc 'Runs all the specs'
task :run_specs do
  puts 'run_specs'
end

Cucumber::Rake::Task.new(:features) do |t|
  t.cucumber_opts = "features --format pretty"
end