Feature: Summary
  In order to quickly and easily see when a build has broken
  As a developer
  I want a summary screen showing the current status of every build pipeline in TeamCity

Scenario: Summary of every build
  Given I have the TeamCity add-on installed in Firefox
  When I browse to the build monitor page
  Then I can see the status of every build in TeamCity