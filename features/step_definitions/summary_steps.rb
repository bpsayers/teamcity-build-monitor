Given /^I have the TeamCity add-on installed in Firefox$/ do
end

When /^I browse to the build monitor page$/ do
  browser.navigate_to 'http://192.168.1.85:8111/build-monitor'
end

Then /^I can see the status of every build in TeamCity$/ do
  browser.web_driver.find_element(:css, 'h1').text.should == 'hello world'
end