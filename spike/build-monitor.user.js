// ==UserScript==
// @name        TeamCityBuildMonitor
// @namespace   TeamCityBuildMonitor
// @include     http://ci.9msn.net/monitor
// @version     1
// @require		http://code.jquery.com/jquery-1.7.2.js
// @require		https://raw.github.com/janl/mustache.js/master/mustache.js
// ==/UserScript==

(function() {
	var $target = $('body');
	var teamCityServer = 'http://ci.9msn.net/guestAuth/app/rest/';

	var get = function (url) {
		return $.ajax({
			cache: false,
			dataType: 'json',
			url: teamCityServer + url
		});
	};

	var getAllBuildConfigurations = function () {
		return get('buildTypes');
	};

	var getAllBuildConfigurationsStatus = function (buildConfigurations) {
		var allBuildConfigurationsStatusPromises = [];
		$.each(buildConfigurations.buildType, function (index, buildConfiguration) {
			var getLatestBuildForBuildConfiguration = function () {
				return get('builds/?locator=lookupLimit:1,buildType:' + buildConfiguration.id);
			};

			var parseBuildConfigurationStatus = function (latestBuildForBuildConfiguration) {
				var buildStatus = latestBuildForBuildConfiguration.count > 0 ?
					latestBuildForBuildConfiguration.build[0].status : 'never run';

				return {
					projectId: buildConfiguration.projectId,
					projectName: buildConfiguration.projectName,
					status:  buildStatus.toLowerCase()
				};
			};

			var buildConfigurationStatusPromise = getLatestBuildForBuildConfiguration().pipe(parseBuildConfigurationStatus);
			allBuildConfigurationsStatusPromises.push(buildConfigurationStatusPromise);
		});

		return $.when.apply(this, allBuildConfigurationsStatusPromises).pipe(function () {
			return $.makeArray(arguments);
		});
	};

	var determineAllProjectsBuildStatus = function (buildConfigurationsWithStatus) {
		var projects = {};

		var insertProject = function (id, name) {
			if (!projects[id]) {
				projects[id] = { name: name, status: 'success' };
			}
		};

		var setProjectStatusToFailure = function (id) {
			projects[id].status = 'failure';
		};

		$.each(buildConfigurationsWithStatus, function (index, buildConfiguration) {
			insertProject(buildConfiguration.projectId, buildConfiguration.projectName);
			if (buildConfiguration.status === 'failure' || buildConfiguration.status === 'error') {
				setProjectStatusToFailure(buildConfiguration.projectId);
			}
		});

		return  {
			projects: $.map(projects, function (project) {
				return project;
			}).sort(function (a, b) {
				if (a.name === b.name) return 0;
				if (a.name < b.name) return -1;
				return 1;
			})
		};
	};

	var showLoadingMarkup = function () {
		$target.empty().append('<div>Loading...</div>');
	};

	var getAllProjectsBuildStatus = function () {
		return getAllBuildConfigurations()
			.pipe(getAllBuildConfigurationsStatus)
			.pipe(determineAllProjectsBuildStatus);
	};

	var displayAllProjectsBuildStatus = function (allProjects) {
		var projectsTemplate = '<style type="text/css">' +
			'body { background-color: black; margin: 0; }' +
			'ul { display: block; list-style: none; margin: 0; padding: 2px 0 0 0; width: 100%; }' +
			'li { float: left; width: 50%; }' +
			'li .name { border-bottom: 3px solid black; color: white; font: bold 30px Arial; height: 1em; overflow: hidden; padding: 10px 0; text-align: center; }' +
			'li:nth-child(odd) .name { border-right: 3px solid black; }' +
			'.success { background-color: green; }' +
			'.failure { background-color: red; }' +
			'</style>' +
			'<ul>{{#projects}}<li><div class="name {{status}}">{{name}}</div></li>{{/projects}}</ul>';
		$target.empty().append(Mustache.render(projectsTemplate, allProjects));
	};

	var displayBuildStatus = function () {
		return getAllProjectsBuildStatus().pipe(displayAllProjectsBuildStatus);
	};

	var refreshBuildStatusIn10Seconds = function () {
		setTimeout(function() {
			displayBuildStatus().always(refreshBuildStatusIn10Seconds);
		}, 10000);
	};

	showLoadingMarkup();
	displayBuildStatus().always(refreshBuildStatusIn10Seconds);
}());