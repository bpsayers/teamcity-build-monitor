class TeamCityWorld
  @@browser = nil

  def initialize
    %x[rake generate_xpi]
  end

  def browser
    @@browser ||= WebBrowser.new
  end

  def close
    if @@browser
      @@browser.close
      @@browser = nil
    end
  end
end
